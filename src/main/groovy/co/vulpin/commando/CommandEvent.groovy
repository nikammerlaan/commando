package co.vulpin.commando

import groovy.transform.CompileStatic
import net.dv8tion.jda.core.EmbedBuilder
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.requests.restaction.MessageAction

import java.awt.*

import static net.dv8tion.jda.core.Permission.MESSAGE_EMBED_LINKS
import static net.dv8tion.jda.core.Permission.MESSAGE_WRITE

@CompileStatic
class CommandEvent {

    @Delegate
    private Message message
    private Commando commando

    CommandEvent(Commando commando, Message message) {
        this.commando = commando
        this.message = message
    }

    MessageAction replyError(String title = null, String message) {
        reply(title, message, Color.RED)
    }

    MessageAction reply(String title = null, String message, Color color = guild?.selfMember?.color) {
        def self = guild?.selfMember
        def tc = textChannel

        def embed = new EmbedBuilder()
            .setTitle(title)
            .setDescription(message)
            .setColor(color)
            .build()

        if(guild && self.hasPermission(tc, MESSAGE_WRITE, MESSAGE_EMBED_LINKS)) {
            return tc.sendMessage(embed)
        } else {
            def chan = author.openPrivateChannel().complete()
            return chan.sendMessage(embed)
        }
    }

    Commando getCommando() {
        return commando
    }

    Message getMessage() {
        return message
    }

}
