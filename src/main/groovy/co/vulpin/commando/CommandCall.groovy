package co.vulpin.commando

import groovy.transform.CompileStatic

import java.util.regex.Matcher

@CompileStatic
class CommandCall {

    private static final String argsRegex = "(?<=[^\\\\]),"

    final Command command
    final Matcher matcher

    CommandCall(Command command, String content) {
        this.command = command
        this.matcher = command.regex.matcher(content)
    }

    String getRawArgs() {
        return command.numArgs > 0 ? matcher.group("args").trim() : ""
    }

    List<String> getArgs() {
        if(command.numArgs == 0)
            return []
        else
            return rawArgs
                    .split(argsRegex, command.numArgs)
                    .collect { it.trim() }
    }

    int getMaxArgs() {
        return rawArgs
                .split(argsRegex)
                .findAll()
                .size()
    }

    int getMatchingGroupCount() {
        def groupCount = (1..matcher.groupCount())
            .collect { try { matcher.group(it) } catch (ignored) {} }
            .findAll()
            .size()

        // decrements the group count if this command has args (because the args group counts and
        // that shouldnt count towards the amount)
        if(args.size() > 0)
            groupCount--

        return groupCount
    }

    boolean matches() {
        matcher.matches() && maxArgs >= command.numArgs
    }

}
