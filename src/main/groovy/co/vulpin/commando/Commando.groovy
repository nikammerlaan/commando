package co.vulpin.commando

import co.vulpin.commando.annotations.Cmd
import co.vulpin.commando.annotations.Options
import co.vulpin.commando.prefix.PrefixSupplier
import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import net.dv8tion.jda.core.entities.Message
import net.dv8tion.jda.core.events.Event
import net.dv8tion.jda.core.events.message.MessageReceivedEvent
import net.dv8tion.jda.core.events.message.MessageUpdateEvent
import net.dv8tion.jda.core.hooks.EventListener
import org.codehaus.groovy.runtime.StackTraceUtils

import java.nio.file.FileSystems
import java.nio.file.Files
import java.nio.file.Path
import java.nio.file.Paths
import java.time.Duration
import java.time.OffsetDateTime
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.function.Supplier
import java.util.stream.Collectors

import static java.nio.file.FileVisitOption.FOLLOW_LINKS

@Slf4j
@CompileStatic
class Commando implements EventListener {

    protected GroovyClassLoader classLoader = new GroovyClassLoader()

    private ExecutorService exec = Executors.newFixedThreadPool(10)

    private List<Command> commands = []

    private PrefixSupplier prefixSupplier
    private String ownerId

    Commando(PrefixSupplier prefixSupplier, String ownerId) {
        this.prefixSupplier = prefixSupplier
        this.ownerId = ownerId

        exec.submit({
            loadCommands()
        })
    }

    @Override
    void onEvent(Event event) {
        if(event instanceof MessageReceivedEvent) {
            handleMessageAsync(event.message)
            return
        }

        if(event instanceof MessageUpdateEvent) {
            def now = OffsetDateTime.now()
            def diff = Duration.between(event.message.creationTime, now)
            if(diff.toSeconds() <= 30) {
                handleMessageAsync(event.message)
            }
        }
    }

    protected void handleMessageAsync(Message message) {
        exec.submit {
            handleMessage(message)
        }
    }

    protected void handleMessage(Message message) {
        if(message.author.isBot())
            return

        if(message.contentRaw == null)
            return

        def content = message.contentRaw.trim()
        def lcContent = content.toLowerCase()

        for(Supplier<String> individualPrefixSupplier : prefixSupplier.getPrefixes(message)) {

            try {
                def prefix = individualPrefixSupplier.get()

                if(prefix == null || !lcContent.startsWith(prefix.toLowerCase()))
                    continue

                content = content.substring(prefix.length()).trim()

                def matching = commands
                    .collect { new CommandCall(it, content) }
                    .findAll { it.matches() }
                    .sort { [ it.matchingGroupCount, it.command.numArgs ] }
                    .reverse()

                if(matching) {
                    def event = new CommandEvent(this, message)
                    executeCommand(matching.first(), event)
                    break
                }
            } catch( e) {
                log.error("An error occurred while matching or executing commands", e)
            }

        }
    }

    protected void executeCommand(CommandCall call, CommandEvent event) {
        log.info("Executing ${call.command}")
        try {
            call.command.execute(event, call.args)
        } catch(e) {
            log.error("An error occurred while executing ${call.command.toString()}", e)
            e = StackTraceUtils.deepSanitize(e)
            event.replyError(e.message).queue()
        }
    }

    void addCommandHolder(Class aClass) {
        if (aClass.simpleName.contains("closure"))
            return

        def anno = aClass.getAnnotation(Options)
        if (anno?.ignore() ?: false)
            return

        aClass.declaredClasses.each {
            addCommandHolder(it)
        }

        List<Command> newCommands = aClass.methods
            .findAll { it.getAnnotation(Cmd) }
            .findAll { it.parameterCount > 0 }
            .findAll { it.parameterTypes[0] == CommandEvent }
            .findAll {
                def opts = it.getAnnotation(Options)
                return !(opts ? opts.ignore() : false)
            }.collect {
                try {
                    return new Command(it)
                } catch (e) {
                    log.error("There was an error loading $it.name of $aClass.name", e)
                    return null
                }
            }.findAll()

        commands += newCommands

        log.debug("Created ${newCommands.size()} from $aClass")
    }

    protected void loadCommands() {
        def uri = this.classLoader.getResource("commando").toURI()

        if(uri.scheme == "jar") {
            def fs = FileSystems.newFileSystem(uri, [:], null)
            def base = fs.getPath("commando")
            loadCommands(base)
            fs.close()
        } else {
            def base = Paths.get(uri)
            loadCommands(base)
        }
    }

    protected void loadCommands(Path base) {
        loadClasses(base.resolve("decorators"))
        loadClasses(base.resolve("commands")).each(this::addCommandHolder)
    }

    protected List<Class> loadClasses(Path path) {
        if(!Files.exists(path))
            return

        return Files.walk(path, FOLLOW_LINKS)
            .filter(Files::isRegularFile)
            .map {
                log.debug("Compiling $it")
                def reader = Files.newBufferedReader(it)
                def name = it.toString()
                try {
                    return classLoader.parseClass(reader, name)
                } catch(e) {
                    log.error("There was an exception while compiling $name", e)
                    return null
                }
            }.filter(Objects::nonNull)
            .collect(Collectors.toList())
    }

    String getOwnerId() {
        return ownerId
    }

    PrefixSupplier getPrefixSupplier() {
        return prefixSupplier
    }

}
