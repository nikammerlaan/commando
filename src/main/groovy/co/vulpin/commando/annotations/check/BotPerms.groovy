package co.vulpin.commando.annotations.check

import co.vulpin.commando.CommandEvent
import com.github.yihtserns.groovy.decorator.Function
import com.github.yihtserns.groovy.decorator.MethodDecorator
import net.dv8tion.jda.core.Permission
import net.dv8tion.jda.core.entities.Channel
import org.codehaus.groovy.transform.GroovyASTTransformationClass

import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target

@MethodDecorator({ Function func, BotPerms botPerms ->
    return { args ->
        def permsArray = botPerms.value()
        def perms = permsArray.toList()
        def event = args[0] as CommandEvent

        perms.removeAll { perm ->
            if(event.guild) {
                def self = event.guild.selfMember
                if(perm.channel) {
                    return self.hasPermission(perm)
                } else {
                    Channel channel = perm.voice ? event.member.voiceState.channel : event.textChannel
                    return self.hasPermission(channel, perm)
                }
            } else {
                def offset = perm.offset
                return offset >= 10 && offset <= 18
            }
        }

        if(perms) {
            def formatted = perms
                .collect {
                    it.toString()
                        .split("_")
                        .collect { it.toLowerCase().capitalize() }
                        .join(" ")
                }
                .sort()
                .join("\n")
            event.reply("I am missing these permissions", formatted).queue()
        } else {
            func.call(args)
        }
    }
})
@GroovyASTTransformationClass("com.github.yihtserns.groovy.decorator.DecoratorASTTransformation")
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@interface BotPerms {
    Permission[] value()
}