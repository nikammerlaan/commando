package co.vulpin.commando.annotations.check

import co.vulpin.commando.CommandEvent
import com.github.yihtserns.groovy.decorator.Function
import com.github.yihtserns.groovy.decorator.MethodDecorator
import org.codehaus.groovy.transform.GroovyASTTransformationClass

import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target

@MethodDecorator({ Function func ->
    return { args ->
        def event = args[0] as CommandEvent
        def ownerId = event.commando.ownerId

        if(event.author.id != ownerId) {
            def message

            def owner = event.JDA.asBot().shardManager.getUserById(ownerId)
            if(owner)
                message = "That command can only be run by **$owner.name#$owner.discriminator**!"
            else
                message = "That command can only be run by the owner of this bot!"

            event.replyError(message).queue()
            return
        }

        func.call(args)
    }
})
@GroovyASTTransformationClass("com.github.yihtserns.groovy.decorator.DecoratorASTTransformation")
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@interface OwnerOnly {}
