package co.vulpin.commando.annotations.check

import co.vulpin.commando.CommandEvent
import com.github.yihtserns.groovy.decorator.Function
import com.github.yihtserns.groovy.decorator.MethodDecorator
import org.codehaus.groovy.transform.GroovyASTTransformationClass

import java.lang.annotation.ElementType
import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy
import java.lang.annotation.Target

import static net.dv8tion.jda.core.Permission.MANAGE_SERVER

@MethodDecorator({ Function func ->
    return { args ->
        def event = args[0] as CommandEvent

        if(event.guild) {
            if(event.member.hasPermission(MANAGE_SERVER))
                func(args)
            else
                event.replyError("You must have the `Manage Server` permission to use this command!").queue()
        } else {
            event.replyError("You can only run this command in a server!").queue()
        }

    }
})
@GroovyASTTransformationClass("com.github.yihtserns.groovy.decorator.DecoratorASTTransformation")
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@interface GuildAdminOnly {}