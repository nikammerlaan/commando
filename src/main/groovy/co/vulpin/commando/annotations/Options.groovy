package co.vulpin.commando.annotations


import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

@Retention(RetentionPolicy.RUNTIME)
@interface Options {
    boolean optional() default false
    boolean ignore() default false
    String[] aliases() default []
}
