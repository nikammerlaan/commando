package co.vulpin.commando.annotations

import groovy.transform.CompileStatic

import java.lang.annotation.Retention
import java.lang.annotation.RetentionPolicy

@CompileStatic
@Retention(RetentionPolicy.RUNTIME)
@interface Cmd {}
