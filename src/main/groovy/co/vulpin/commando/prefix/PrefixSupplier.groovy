package co.vulpin.commando.prefix

import groovy.transform.CompileStatic
import net.dv8tion.jda.core.entities.Message

import java.util.function.Supplier

@CompileStatic
interface PrefixSupplier {

    List<Supplier<String>> getPrefixes(Message message)

}
