package co.vulpin.commando

import co.vulpin.commando.annotations.Options
import net.dv8tion.jda.core.entities.MessageEmbed
import org.codehaus.groovy.runtime.StackTraceUtils

import java.lang.reflect.AnnotatedElement
import java.lang.reflect.Method
import java.util.regex.Pattern

class Command {

    final Pattern regex

    private Method method
    private Object instance

    protected Command(Method method) {
        this.method = method
        this.instance = method.declaringClass.newInstance()

        this.regex = createRegex()
    }

    int getNumArgs() {
        return method.parameterCount - 1
    }

    void execute(CommandEvent event, List<String> args) {
        def methodArgs = [event, args].flatten()
        try {
            def result = method.invoke(instance, *methodArgs)
            if(result) {
                if(result instanceof MessageEmbed)
                    event.channel.sendMessage(result).queue()
                else
                    event.reply(result as String).queue()
            }
        } catch (e) {
            e = StackTraceUtils.sanitizeRootCause(e)
            e.printStackTrace()
            event.replyError(e.message).queue()
        }
    }

    private Pattern createRegex() {
        List<AnnotatedElement> layers = []
        layers += method

        def parent = method.declaringClass
        while(parent) {
            layers += parent
            parent = parent.declaringClass
        }

        layers.reverse(true)

        def regex = ""

        regex += "(?is)"

        regex += layers.collect {
            def anno = it.getAnnotation(Options)

            List<String> names = []

            if(it instanceof Method)
                names.add(it.name)
            else if(it instanceof Class)
                names.add(it.simpleName)

            if(anno?.aliases())
                names.addAll(anno.aliases())

            def group = "(${names.collect { Pattern.quote(it) }.join("|")})"

            if(anno?.optional())
                group += "?"

            return group
        }.join("\\s*") + ""

        if(numArgs > 0)
            regex += "(?:\\s+|^)(?<args>.*)"

        return Pattern.compile(regex)
    }

    String toString() {
        def names = []
        names += method.name

        def parent = method.declaringClass
        while(parent) {
            names.add(0, parent.simpleName)
            parent = parent.declaringClass
        }

        return names.join(".")
    }

}
