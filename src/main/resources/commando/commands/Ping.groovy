package commando.commands

import co.vulpin.commando.CommandEvent
import co.vulpin.commando.annotations.Cmd
import co.vulpin.commando.annotations.Options
import co.vulpin.commando.annotations.check.BotPerms

import java.time.Duration

import static net.dv8tion.jda.core.Permission.MESSAGE_WRITE

class Ping {

    @Cmd
    @Options(optional = true)
    @BotPerms([MESSAGE_WRITE])
    void pong(CommandEvent event) {
        event.channel.sendMessage("Calculating ping...").queue({
            def diff = Duration.between(event.message.creationTime, it.creationTime).toMillis()
            it.editMessage("${diff}ms").queue()
        })
    }

}
