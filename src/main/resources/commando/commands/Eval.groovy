package commando.commands

import co.vulpin.commando.CommandEvent
import co.vulpin.commando.annotations.Cmd
import co.vulpin.commando.annotations.Options
import co.vulpin.commando.annotations.check.BotPerms
import co.vulpin.commando.annotations.check.OwnerOnly
import net.dv8tion.jda.core.EmbedBuilder
import org.codehaus.groovy.runtime.StackTraceUtils

import java.awt.*

import static net.dv8tion.jda.core.Permission.MESSAGE_EMBED_LINKS

class Eval {

    @Cmd
    @Options(optional = true)
    @OwnerOnly
    @BotPerms([MESSAGE_EMBED_LINKS])
    void code(CommandEvent event, String code) {
        def binding = new Binding([
            event: event,
            message: event.message,
            bot: event.JDA.asBot().shardManager,
            jda: event.JDA
        ])

        def shell = new GroovyShell(binding)

        try {

            def resp = shell.evaluate(code) as String
            def embed = new EmbedBuilder().with {
                description = resp?.take(2000)
                color = Color.GREEN
                build()
            }
            event.channel.sendMessage(embed).queue()

        } catch (e) {

            e = StackTraceUtils.deepSanitize(e)

            def sw = new StringWriter()
            def pw = new PrintWriter(sw)
            e.printStackTrace(pw)
            // limits it to 1950 to build in some padding for the backticks + language
            def stackTrace = sw.toString().take(1950)
            def text = "```groovy\n$stackTrace```"
            event.channel.sendMessage(text).queue()

        }

    }

}
